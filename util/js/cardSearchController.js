(function() {
    cardSearchApp.controller('CardSearchController', ['$scope', '$http', '$sce', function($scope, $http, $sce) {

        $scope.CONSTANT = {
        	TARGET: {
				'self': '自身',
				'caster': '術者',
				'one': '1体',
				'all': '全員',
				'friend': '味方1体',
				'friends': '味方全員',
				'enemy': '敵1体',
				'enemies': '敵全員',
				'area': 'エリア',
			},
            ICON: {
                '無': 'http://scstcg.net/util/img/card/iconPlain.png',
                '符': 'http://scstcg.net/util/img/card/iconPlain.png',
                '神': 'http://scstcg.net/util/img/card/iconGod.png',
                '魔': 'http://scstcg.net/util/img/card/iconMagic.png',
                '霊': 'http://scstcg.net/util/img/card/iconGhost.png',
                '妖': 'http://scstcg.net/util/img/card/iconEvil.png',
                'S': 'http://scstcg.net/util/img/card/iconScore.png',
                'C': 'http://scstcg.net/util/img/card/iconCard.png',
            },
            PROPERTY: [
                { name:'無', value:'normal' },
                { name:'神', value:'god' },
                { name:'魔', value:'magic' },
                { name:'霊', value:'ghost' },
                { name:'妖', value:'evil' },
            ],
            COST: [
                { name:'符', value:'normal' },
                { name:'神', value:'god' },
                { name:'魔', value:'magic' },
                { name:'霊', value:'ghost' },
                { name:'妖', value:'evil' },
                { name:'S', value:'score' },
                { name:'C', value:'card' },
            ],
            RANK: [1, 2, 3, 4, 5, 6, 7],
            ORG: [
                { name: 'すべて', value: undefined },
                { name: '博麗神社', value: '博麗神社' },
                { name: '守矢神社', value: '守矢神社' },
                { name: '紅魔館', value: '紅魔館' },
                { name: '永遠亭', value: '永遠亭' },
                { name: '迷い家', value: '迷い家' },
                { name: '命蓮寺', value: '命蓮寺' },
                { name: '地霊殿', value: '地霊殿'},
                { name: '神霊廟', value: '神霊廟' },
                { name: '人里', value: '人里' },
                { name: '魔法の森', value: '魔法の森' },
                { name: '冥界', value: '冥界' },
                { name: '妖怪の山', value: '妖怪の山' }
            ],
            CARD_TYPE: [
                { name: 'すべて', value: undefined },
                { name: 'スペルカード', value: 'スペルカード' },
                { name: 'オプション', value: 'オプション' }
            ],
            RARITY: [
                { name: 'すべて', value: undefined },
                { name: 'COMMON', value: 'C' },
                { name: 'UNCOMMON', value: 'UC' },
                { name: 'RARE', value: 'R' },
                { name: 'SUPER RARE', value: 'SR' }
            ],
            RARITY_MAP: {
                C : 'COMMON',
                UC: 'UNCOMMON',
                R : 'RARE',
                SR: 'SUPER RARE'
            },
            SPELL_TYPE: [
                { name: 'すべて', value: undefined },
                { name: 'なし', value: 'なし' },
                { name: '装備', value: '装備' },
                { name: '炎', value: '炎' },
                { name: '音', value: '音' },
                { name: '光', value: '光' },
                { name: '人形', value: '人形' },
                { name: '水', value: '水' },
                { name: '星', value: '星' },
                { name: '精神', value: '精神' },
                { name: '地', value: '地' },
                { name: '毒', value: '毒' },
                { name: '氷', value: '氷' },
                { name: '風', value: '風' },
                { name: '雷', value: '雷' }
            ],
            RACE: [
                { name: 'すべて', value: undefined },
                { name: '悪魔', value: '悪魔' },
                { name: '河童', value: '河童' },
                { name: '鬼', value: '鬼' },
                { name: '月人', value: '月人' },
                { name: '獣', value: '獣' },
                { name: '神', value: '神' },
                { name: '人間', value: '人間' },
                { name: '仙人', value: '仙人' },
                { name: '天狗', value: '天狗' },
                { name: '天人', value: '天人' },
                { name: '付喪神', value: '付喪神' },
                { name: '魔法使い', value: '魔法使い' },
                { name: '幽霊', value: '幽霊' },
                { name: '妖怪', value: '妖怪' },
                { name: '妖精', value: '妖精' }
            ],
            SUMMON_COST: [
                { name: 'すべて', value: "all" },
                { name: 'なし', value: undefined },
                { name: '符', value: ['符'] },
                { name: '符符', value: ['符','符'] },
                { name: '神', value: ['神'] },
                { name: '魔', value: ['魔'] },
                { name: '霊', value: ['霊'] },
                { name: '妖', value: ['妖'] }
            ],
        }
        $scope.selected_ranks = angular.copy($scope.CONSTANT.RANK);
        $scope.selected_costs = new Array();
        $scope.selected_properties = angular.copy($scope.CONSTANT.PROPERTY);
        $scope.selected_summon_costs = $scope.CONSTANT.SUMMON_COST[0].value;

        $http.jsonp('http://scstcg.floe-net.jp/system/api/readcarddata?callback=JSON_CALLBACK').success(function(data) {
            $scope.cardCatalog = data;
 		});

        $scope.cardSelected = function(card) {
            $scope.selected_card = angular.copy(card);
            if ( $scope.selected_card.errata ) $scope.selected_card.errata = $sce.trustAsHtml($scope.selected_card.errata.replace(/\n/g, '<br />'));
            if ( $scope.selected_card.skill_a ) {
	            $scope.selected_card.skill_a.forEach(function(it){
		            if ( typeof(it.text) == "string" ) it.text = $sce.trustAsHtml(it.text.replace(/\n/g, '<br />'));
		        });
		    }
            if ( $scope.selected_card.skill_s ) {
	            $scope.selected_card.skill_s.forEach(function(it){
		            if ( typeof(it.text) == "string" ) it.text = $sce.trustAsHtml(it.text.replace(/\n/g, '<br />'));
		        });
		    }
            if ( $scope.selected_card.skill_e ) {
	            $scope.selected_card.skill_e.forEach(function(it){
		            if ( typeof(it.text) == "string" ) it.text = $sce.trustAsHtml(it.text.replace(/\n/g, '<br />'));
		        });
		    }
		    if ( $scope.selected_card.qa ) {
	            $scope.selected_card.qa.forEach(function(it){
		            if ( typeof(it.q) == "string" ) it.q = $sce.trustAsHtml(it.q.replace(/\n/g, '<br />'));
		            if ( typeof(it.a) == "string" ) it.a = $sce.trustAsHtml(it.a.replace(/\n/g, '<br />'));
		        });
		    }
        };

        $scope.filtered = new Array();

		$scope.find_card_index = function (arr, val)
		{
			for (var i=0; i<arr.length; i++) {
				if (arr[i].no === val.no)
					return i;
			}

			return -1;
		}
    }]);
}());
