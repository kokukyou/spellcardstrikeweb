//スムーズスクロール
$(function(){
	$('a[href^=#]').click(function(){
		var speed = 300;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});

//スクロール追従左ナビ
$(document).ready(function() {
	var scrStd = 193;
	$(window).scroll(function(){
		var scrH = $(window).scrollTop();
		if(scrH > scrStd) {
			$('#main #sub.scroll').addClass('fixed');
		} else {
			$('#main #sub.scroll').removeClass('fixed');
		}
	})
});

//カード検索

$(document).ready(function() {
	$('#searchBox dt').click(function(){
		$(this).toggleClass('open').next().slideToggle('fast');
	})
});

//メニュー開閉
$(document).ready(function(){
  $('.openable > h4').next().hide();
  $('.openable > h4').click(function(){
    // 引数には開閉する速度を指定します
    $(this).next().slideToggle('slow');
  });
});

//カード検索属性トグル