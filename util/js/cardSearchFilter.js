(function() {
    cardSearchApp.filter('property', function() {
        return function(input, values) {
            if (!_.isArray(values)) { return; }
            var result_list =  _.filter(input, function(it) {
                return values.some(function(value) {
                    return (it.property.indexOf(value.name)>=0);
                });
            });
            
            return result_list;
        };
    });
}());
(function() {
    cardSearchApp.filter('rank', function() {
        return function(input, values) {
            if (!_.isArray(values)) { return; }
            var result_list =  _.filter(input, function(it) {
                return values.some(function(value) {
                    return (it.rank == value);
                });
            });
            
            return result_list;
        };
    });
}());

function find_cost_from(skill_array, value){
	return skill_array.some(function(skill){
		return skill.self_cost == value || (skill.costs != undefined && (skill.costs.indexOf(value)>=0)) || (skill.variable_cost != undefined && (skill.variable_cost.indexOf(value)>=0));
	});
}
(function() {
    cardSearchApp.filter('cost', function() {
        return function(input, values) {
			if (!_.isArray(values)) { return input; }
			if (values.length <= 0) { return input; }
            var result_list = _.filter(input, function(it) {
                return values.some(function(value) {
                	return (find_cost_from(it.skill_a, value.name) || find_cost_from(it.skill_s, value.name));
                });
            });
            
            return result_list;
        };
    });
}());
(function() {
    cardSearchApp.filter('summon_cost', function() {
        return function(input, values) {
			if (values == "all") { return input; }
            var result_list = _.filter(input, function(it) {
                return angular.equals(it.cost, values);
            });
            
            return result_list;
        };
    });
}());
